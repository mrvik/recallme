# recallme(1)
Emit a notification on a specific time or after some seconds

## Commandline options

```
recallme time|+seconds "Notification title" "Notification description"
```

## Building

### Dependencies
- Runtime
    - libnotify
    - C Standard Library (like glibc)
    - glib2
- Compile
    - Dev packages from the runtime dependencies
    - C compiler (like gcc)
    - make
    - pkg-config (usually the package is named pkgconf or pkg_conf)

### Building it

```bash
# Compile code
make

# Install to /usr/local
make install

# Install to /usr instead of /usr/local
make PREFIX=/usr install

# Install to a custom directory (e.g. for packaging)
make PREFIX=/usr DESTDIR="${pkgdir}" install
```

After installing, a man page is available for recallme(1). If you installed it with the default PREFIX, make sure /usr/local/share/man is in the MANPATH
