CC := /usr/bin/gcc

DESTDIR:=/
PREFIX:=/usr/local
DST=${DESTDIR}${PREFIX}
CFLAGS:=
LDFLAGS:=
C_FLAGS+=-Wall
C_FLAGS+=$(shell pkg-config --cflags --libs libnotify)
C_FILES=$(shell find -maxdepth 1 -name "*.c")

build: recallme.c lib/functions.c
	$(CC) -o recallme $(CFLAGS) $(C_FLAGS) $(C_FILES) $(LDFLAGS)

clean:
	$(RM) recallme

install:
	mkdir -p "${DST}/bin" "${DST}/share/man/man1"
	install -D -m755 recallme "${DST}/bin/"
	install -Dm644 recallme.1.groff "${DST}/share/man/man1/recallme.1"

uninstall:
	$(RM) "${DST}/bin/recallme"
	$(RM) "${DST}/share/man/man1/recallme.1"
