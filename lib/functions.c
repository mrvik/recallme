#define _XOPEN_SOURCE
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

int parse_time_parameter(int*, char*);
struct tm nowtm();
bool string_has_char_on(int, char*, char);

bool string_has_char_on(int position, char *str, char ch){
    if(sizeof(*str)<=position){
        fprintf(stderr, "String had %lu characters and asked for %d!\n",(unsigned long)sizeof(*str), position);
        return false;
    }
    char there=str[position];
    return there==ch;
}

int parse_time_parameter(int* seconds, char* parameter){
    struct tm tmstr;
    time_t actualtime, timestr;
    //Parse time and convert diff to seconds (int)
    {
        char *tcstr=parameter;
        if(string_has_char_on(0, parameter, '+')){
            int secs=(int)strtoumax(tcstr, NULL, 10);
            if(secs>0){
                *seconds=secs;
                return 0;
            }else{
                fprintf(stderr, "Bad wait time specification: %s. Number conversion was %d. Not acceptable\n", tcstr, secs);
                return -1;
            }
        }
    }
    tmstr=nowtm(); //Starting from the actual time. strptime modifies the tm struct with the values provided
    struct tm actualtm=nowtm();
    tmstr.tm_sec=0;tmstr.tm_min=0;tmstr.tm_hour=0; //Reset variables set by strptime
    strptime(parameter, "%H:%M:%S", &tmstr); //Modify tmstr with contents on parameter
    if(actualtm.tm_hour>tmstr.tm_hour||(actualtm.tm_hour>=tmstr.tm_hour&&actualtm.tm_min>tmstr.tm_min)){
        printf("See u tomorrow!\n");
        tmstr.tm_mday++; //Add a day if target hour is lower than the current one
    }
    actualtime=mktime(&actualtm); //As a side effect, this call can correct out-of-range errors when adding time to tm (2019/12/32)
    timestr=mktime(&tmstr); //See above comment
    int rs=(int)(timestr-actualtime);
    *seconds=rs;
    return 0;
}

struct tm nowtm(){
    time_t null;
    struct tm tm;
    memset(&tm, 0, sizeof(struct tm));
    time(&null);
    tm=*localtime(&null);
    return tm;
}
