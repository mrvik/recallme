#define _XOPEN_SOURCE
#include <libnotify/notify.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include "lib/functions.c"

#define APPNAME "Recall me"

typedef struct Arguments{
    int seconds;
    char *title;
    char *description;
    bool fork;
}Arguments;

int parse_arguments(int, char*[], Arguments*);

int main(int argc, char *argv[]){
    notify_init(APPNAME);
    Arguments args={ .seconds= 0, .title= "", .description= ""};
    int st=parse_arguments(argc, argv, &args);
    if(st!=0){
        return st;
    }
    if(args.fork){
        int pid=fork();
        switch(pid){
            case -1:
                fprintf(stderr, "Error while forking to backgorund\n");
                return EXIT_FAILURE;
            case 0:
                break;
            default:
                printf("Forked to background. %d\n", pid);
                goto exit;
        }
    }
    NotifyNotification *notification=notify_notification_new(args.title, args.description, "dialog-information");
    printf("Waiting %d second%c\n", args.seconds, args.seconds==1?' ':'s');
    if(args.seconds>0){
        sleep(args.seconds);
    }
    notify_notification_show(notification, NULL);
    g_object_unref(G_OBJECT(notification));
    exit:
    notify_uninit();
    return EXIT_SUCCESS;
}

int parse_arguments(int argc, char *argv[], Arguments *rcvr){
    int current=1;

    //Check number of parameters
    if(argc<3){
        rtfm:
        fprintf(stderr, "RTFM!\n");
        return -1;
    }
    //If first parameter is -b set fork option
    if(strcmp(argv[current], "-b") == 0){
        rcvr->fork=true;
        current++;
        if(argc<4){
            goto rtfm;
        }
    }
    //Parse time parameter
    int res=parse_time_parameter(&rcvr->seconds, argv[current]);
    if(res != 0){
        return res;
    }
    current++;
    //Insert title and description (if present) into rcvr
    rcvr->title=argv[current++]; //This is safe
    if(argc-current >= 0){
        rcvr->description=argv[current++];
    }
    if(argc-current > 0){
        fprintf(stderr, "I'll ignore the rest of your bs! You passed %d args!\n", argc-1);
    }

    return 0;
}
